Property Analyzer
 version  version  version  version  version  version  version  version  version  version  version  version  version  version  version 
Overview
-------------
This app scraped data from https://www.sothebysrealty.com/eng/sales/usa
and view graphs in dashboard.

It can
1) Scrape Data from https://www.sothebysrealty.com/eng/sales/usa
2) State, Address and Price Table of scraped data
3) State vs Prices bar chart
4) Price Upon Request Vs Prices given pie chart

Code Dependencies
-------
None

Libraries
-------
* asgiref version 3.3.4
* certifi version 2020.12.5
* chardet version 4.0.0
* Django version 3.2
* django-crispy-forms version 1.11.2
* idna version 2.10
* pytz version 2021.1
* requests version 2.25.1
* selenium version 3.141.0
* sqlparse version 0.4.1
* typing-extensions version 3.7.4.3
* urllib3 version 1.26.4


Installation
-------
1. Make virtual environment inside project folder
```bash
python -m venv env
```
2. Activate virtual environment
```bash
ubuntu: source env/bin/activate
windows: env\Scripts\activate.bat
```
3. Install requirememnts.txt
```bash
pip install -r requirements.txt
```
4. Install Migrations (Inside efuture folder)
```bash
python manage.py makemigrations
python manage.py migrate
```
5. Virtual Server (Inside efuture folder)
```bash
python manage.py runserver
```
settings.py
-----------

Name | Description
--- | --- 
WEBDRIVER_PATH | Add the path to chrome driver for scraping
