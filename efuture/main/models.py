from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class ToDoList(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="todolist", null=True)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Item(models.Model):
    todoList = models.ForeignKey(ToDoList, on_delete=models.CASCADE)
    text = models.CharField(max_length=300)
    complete = models.BooleanField()

    def __str__(self):
        return self.text

class Property(models.Model):
    state = models.CharField(max_length=255)
    address = models.CharField(max_length=500)
    price = models.CharField(max_length=200)
    features = models.CharField(max_length=500, default=None, blank=True, null=True)

    def __str__(self):
        return self.state, self.address, self.price