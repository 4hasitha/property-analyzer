from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
from . models import Property
from django.conf import settings

# Create your views here.
def dashboard(response):
    properties = Property.objects.all()
    prices = []
    states = []
    price_upon_request = 0
    prices_given = 0
    for property in properties:
        try:
            prices.append(int(property.price.replace("$","").replace(",","")))
            states.append(property.state)
            prices_given = prices_given+1
        except:
            price_upon_request = price_upon_request + 1

    return render(response, "property/dashboard.html", {"prices":prices, "states":states,
                                                        "prices_given":prices_given,
                                                        "price_upon_request":price_upon_request})

def manageData(response):
    if response.method == "POST":
        Property.objects.all().delete()

        url = "https://www.sothebysrealty.com/eng/sales/usa"
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("start-maximized")
        chrome_options.add_argument("disable-infobars")
        chrome_options.add_argument("--disable-extensions")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--no-sandbox")
        driver = webdriver.Chrome(getattr(settings, "WEBDRIVER_PATH", None), options=chrome_options)

        time.sleep(2)
        driver.get(url)
        time.sleep(5)
        scrape_blocks = driver.find_elements_by_xpath(
            "//div[@class='Results-card__wrapper  u-no-decoration u-cursor-pointer']")
        for count in range(len(scrape_blocks)):
            stateSave = driver.find_elements_by_xpath(
                "//h4[@class='h5 Results-card__body-address u-features-title-small u-color-sir-blue']")[count].text
            addressSave = driver.find_elements_by_xpath(
                "//h5[@class='h5 Results-card__body-address u-features-title-small u-color-sir-blue']")[count].text
            priceSave = driver.find_elements_by_xpath("//h6[@class='Results-card__body-price']")[count].text
            featuresSave = driver.find_elements_by_xpath("//ul[@class='Results-card__feat-list']")[count].text
            prop = Property(state=stateSave, address=addressSave, price=priceSave, features=featuresSave)
            prop.save()


        driver.quit()

    properties = Property.objects.all()


    return render(response, "property/manage_data.html", {"properties":properties})

