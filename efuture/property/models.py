from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Property(models.Model):
    state = models.CharField(max_length=255)
    address = models.CharField(max_length=500)
    price = models.CharField(max_length=200)
    features = models.CharField(max_length=500, default=None, blank=True, null=True)
